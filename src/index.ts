#!/usr/bin/env node

import * as fs from 'fs'
import * as chalk from 'chalk'
import * as toml from '@iarna/toml'
import { homedir } from 'os'
import Table from 'cli-table3'
import * as readlineSync from 'readline-sync'
import { execSync } from 'child_process'

type OnceItem = {
	letter?: string
	title: string
	date: string
	defaultPriority: string
	defaultStatus: string
	defaultNote: string
}

type TwoWeekItem = {
	letter?: string
	title: string
	startDate: string
	defaultPriority: string
	defaultStatus: string
	defaultNote: string
}

type MonthItem = {
	letter?: string
	title: string
	day: number
	defaultPriority: string
	defaultStatus: string
	defaultNote: string
}

type YearItem = {
	letter?: string
	title: string
	month: number
	day: number
	defaultPriority: string
	defaultStatus: string
	defaultNote: string
}

type RecordItem = {
	letter?: string
	status: string
	title: string
	due: string
	completed: string
	priority: string
	note: string
}

class BillProg {
	oDefaultYearItems: YearItem[] = [
		{
			title: 'test yearly bill',
			month: 10,
			day: 16,
			defaultPriority: '2',
			defaultStatus: '',
			defaultNote: '',
		},
		{
			title: 'Pay Property Tax',
			month: 11,
			day: 12,
			defaultPriority: '!',
			defaultStatus: '',
			defaultNote: '',
		},
	]
	oDefaultMonthItems: MonthItem[] = [
		{
			title: 'Pay Gas Bill',
			day: 14,
			defaultPriority: '9',
			defaultStatus: '',
			defaultNote: '',
		},
		{
			title: 'Staff Meeting',
			day: 13,
			defaultPriority: '!',
			defaultStatus: '',
			defaultNote: '',
		},
	]
	oDefault2WeekItems: TwoWeekItem[] = [
		{
			title: 'Payday! Check account for deposit.',
			startDate: '2018-09-29',
			defaultPriority: '2',
			defaultStatus: '',
			defaultNote: '',
		},
		{
			title: 'Buy new boat',
			startDate: '2018-04-02',
			defaultPriority: '10',
			defaultStatus: '',
			defaultNote: '',
		},
	]

	oDefaultOnceItems: OnceItem[] = [
		{
			title: 'test one time item',
			date: '2019-09-21',
			defaultPriority: '',
			defaultStatus: '',
			defaultNote: '',
		},
		{
			title: 'Project Codename Avocado',
			date: '2019-03-09',
			defaultPriority: '',
			defaultStatus: '',
			defaultNote: '',
		},
	]

	packageDetails = require('../package.json')

	strGroup: string = ''
	strDataTomlFolder: string = homedir() + '/bixfliz-tasklog-cli-data'
	intDisplayYear: number = new Date().getFullYear()
	intDisplayMonth: number = new Date().getMonth() + 1
	strRecordsTomlFilename: string

	strRecurringTomlFilename: string = this.strDataTomlFolder + '/config.toml'

	oRecords: RecordItem[] = new Array()
	oYearItems: YearItem[] = new Array()
	oMonthItems: MonthItem[] = new Array()
	o2WeekItems: TwoWeekItem[] = new Array()
	oOnceItems: OnceItem[] = new Array()
	strAlternativePriorityTitle: string = 'Priority'
	strTotalAlternativePriorityValue: string = 'false'
	strTotalAlternativePriorityFormatAsMoney: string = 'false'
	strAllowSFTPUploadAndDownload: string = 'false'
	strSFTPUsername: string = 'myusername'
	strSFTPServerName: string = 'example.com'
	strSFTPServerFolder: string = 'tasklog-data'
	strArrGroupList: string[] = []

	public constructor() {
		console.log(this.packageDetails.displayName + ' v' + this.packageDetails.version)
		this.loadRecurringData().then(() => {
			this.loadAndShowMonthTable(this.intDisplayYear, this.intDisplayMonth).then(() => {})
		})
	}

	async showTable() {
		for (var intEachItem = 0; intEachItem < this.oYearItems.length; intEachItem++) {
			let oRow = this.oYearItems[intEachItem]
			let booItemAlreadyInList: boolean = false
			if (oRow.month == this.intDisplayMonth) {
				for (let oRec of this.oRecords) {
					if (oRec.title == oRow.title) {
						booItemAlreadyInList = true
						break
					}
				}

				if (booItemAlreadyInList) continue

				this.oRecords.push({
					due:
						this.intDisplayYear.toString() +
						'-' +
						oRow.month.toString().padStart(2, '0') +
						'-' +
						oRow.day.toString().padStart(2, '0'),
					completed: '',
					status: oRow.defaultStatus,
					title: oRow.title,
					priority: oRow.defaultPriority,
					note: oRow.defaultNote,
				})
			}
		}

		for (var intEachItem = 0; intEachItem < this.oMonthItems.length; intEachItem++) {
			let oRow = this.oMonthItems[intEachItem]
			let booItemAlreadyInList: boolean = false
			for (let oRec of this.oRecords) {
				if (oRec.title == oRow.title) {
					booItemAlreadyInList = true
					break
				}
			}
			if (booItemAlreadyInList) continue
			this.oRecords.push({
				due:
					this.intDisplayYear.toString() +
					'-' +
					this.intDisplayMonth.toString().padStart(2, '0') +
					'-' +
					oRow.day.toString().padStart(2, '0'),
				completed: '',
				status: oRow.defaultStatus,
				title: oRow.title,
				priority: oRow.defaultPriority,
				note: oRow.defaultNote,
			})
		}

		// let dtmCurrentStart: Date = new Date(this.intDisplayYear, this.intDisplayMonth - 1, 1)
		// let dtmCurrentEnd: Date = new Date(this.intDisplayYear, this.intDisplayMonth, 0)

		let dtmCurrentStart: Date = new Date(
			Date.parse(this.intDisplayYear.toString() + '-' + this.intDisplayMonth.toString() + '-01'),
		)
		let dtmCurrentEnd: Date = new Date(
			Date.parse(this.intDisplayYear.toString() + '-' + (this.intDisplayMonth + 1).toString() + '-01'),
		)
		dtmCurrentEnd.setDate(dtmCurrentEnd.getDate() - 1)

		for (var intEachItem = 0; intEachItem < this.o2WeekItems.length; intEachItem++) {
			let oRow = this.o2WeekItems[intEachItem]
			let booItemAlreadyInList: boolean = false
			for (let oRec of this.oRecords) {
				if (oRec.title == oRow.title) {
					booItemAlreadyInList = true
					break
				}
			}
			if (booItemAlreadyInList) continue
			let dtmEachDate: Date = new Date(Date.parse(oRow.startDate))
			do {
				if (dtmEachDate >= dtmCurrentStart && dtmEachDate <= dtmCurrentEnd) {
					this.oRecords.push({
						due:
							this.intDisplayYear.toString() +
							'-' +
							this.intDisplayMonth.toString().padStart(2, '0') +
							'-' +
							dtmEachDate.getUTCDate().toString().padStart(2, '0'),
						status: oRow.defaultStatus,
						completed: '',
						title: oRow.title,
						priority: oRow.defaultPriority,
						note: oRow.defaultNote,
					})
				}
				dtmEachDate.setUTCDate(dtmEachDate.getUTCDate() + 14)
			} while (dtmEachDate <= dtmCurrentEnd)
		}

		let booItemAlreadyInList: boolean = false
		for (var intEachItem = 0; intEachItem < this.oOnceItems.length; intEachItem++) {
			let oRow = this.oOnceItems[intEachItem]
			booItemAlreadyInList = false
			for (let oRec of this.oRecords) {
				if (oRec.title == oRow.title) {
					booItemAlreadyInList = true
					break
				}
			}
			if (booItemAlreadyInList) continue
			let dtmEachDate: Date = new Date(Date.parse(oRow.date))
			if (dtmEachDate >= dtmCurrentStart && dtmEachDate <= dtmCurrentEnd) {
				this.oRecords.push({
					due:
						this.intDisplayYear.toString() +
						'-' +
						this.intDisplayMonth.toString().padStart(2, '0') +
						'-' +
						dtmEachDate.getUTCDate().toString().padStart(2, '0'),
					status: oRow.defaultStatus,
					completed: '',
					title: oRow.title,
					priority: oRow.defaultPriority,
					note: oRow.defaultNote,
				})
			}
		}

		if (this.oRecords && this.oRecords.length && this.oRecords.length > 1) {
			this.oRecords.sort(
				(b, a) => (a.due + a.status < b.due + b.status ? -1 : a.due + a.status > b.due + b.status ? 1 : 0),
			)
		}

		const oCliTable = new Table({
			head: [ '', 'Due', 'Done', 'x', 'Title', this.strAlternativePriorityTitle, 'Note' ],
			colWidths: [ 3, 18, 18, 3, 22, 14, 40 ],
			wordWrap: true,
		}) as Table.HorizontalTable

		let numTotalPriority: number = 0
		for (var intEachRecord = 0; intEachRecord < this.oRecords.length; intEachRecord++) {
			let oRow = this.oRecords[intEachRecord]
			let dtmDate: Date = new Date(Date.parse(oRow.due))
			let strDateCompleted: string = ''
			let strDate: string =
				(dtmDate.getUTCMonth() + 1).toString() +
				'/' +
				dtmDate.getUTCDate().toString() +
				'/' +
				this.intDisplayYear.toString() +
				' ' +
				this.dayName(dtmDate)
			if (oRow.completed) {
				let dtmDateCompleted: Date = new Date(Date.parse(oRow.completed))
				strDateCompleted =
					(dtmDateCompleted.getUTCMonth() + 1).toString() +
					'/' +
					dtmDateCompleted.getUTCDate().toString() +
					'/' +
					this.intDisplayYear.toString() +
					' ' +
					this.dayName(dtmDateCompleted)
			}
			this.oRecords[intEachRecord].letter = String.fromCharCode(97 + intEachRecord)

			if (!strDateCompleted && oRow.status == '!') {
				oCliTable.push([
					String.fromCharCode(97 + intEachRecord),
					strDate,
					strDateCompleted,
					oRow.status,
					chalk.default.black.bgRed(oRow.title),
					{ hAlign: 'right', content: oRow.priority },
					oRow.note,
				])
			} else if (strDateCompleted) {
				oCliTable.push([
					chalk.default.dim.grey.strikethrough.italic(String.fromCharCode(97 + intEachRecord)),
					chalk.default.dim.grey.strikethrough.italic(strDate),
					chalk.default.dim.grey.strikethrough.italic(strDateCompleted),
					chalk.default.dim.grey.strikethrough.italic(oRow.status),
					chalk.default.dim.grey.strikethrough.italic(oRow.title),
					{ hAlign: 'right', content: chalk.default.dim.grey.strikethrough.italic(oRow.priority) },
					chalk.default.dim.grey.strikethrough.italic(oRow.note),
				])
			} else {
				oCliTable.push([
					String.fromCharCode(97 + intEachRecord),
					strDate,
					strDateCompleted,
					oRow.status,
					oRow.title,
					{ hAlign: 'right', content: oRow.priority },
					oRow.note,
				])
			}
			if (this.strTotalAlternativePriorityValue == 'true' && oRow.priority) {
				let strNum: string = oRow.priority
				strNum = strNum.replace(/[\$]/g, '')
				strNum = strNum.replace(/[\,]/g, '')
				strNum = strNum.replace(/ /g, '')
				let numNumber: number = Number.parseFloat(strNum)
				if (!isNaN(numNumber)) numTotalPriority += numNumber
			}
		}
		console.log(chalk.default.red.bold.underline('\n\n' + this.monthName(this.intDisplayMonth)))
		console.log(oCliTable.toString())
		const formatter = new Intl.NumberFormat('en-US', {
			style: 'currency',
			currency: 'USD',
			minimumFractionDigits: 2,
		})

		if (
			this.strTotalAlternativePriorityValue == 'true' &&
			this.strTotalAlternativePriorityFormatAsMoney == 'true'
		) {
			console.log('Total ' + this.strAlternativePriorityTitle + ': ' + formatter.format(numTotalPriority) + '\n')
		} else if (this.strTotalAlternativePriorityValue == 'true') {
			console.log('Total ' + this.strAlternativePriorityTitle + ': ' + numTotalPriority.toString() + '\n')
		}
	}

	async loadRecurringData() {
		try {
			if (!fs.existsSync(this.strDataTomlFolder + '/')) {
				fs.mkdirSync(this.strDataTomlFolder + '/')
			}

			this.strRecurringTomlFilename = this.strDataTomlFolder + '/config' + this.strGroup.toLowerCase() + '.toml'

			if (!fs.existsSync(this.strRecurringTomlFilename)) {
				fs.closeSync(fs.openSync(this.strRecurringTomlFilename, 'w'))
				fs.writeFileSync(
					this.strRecurringTomlFilename,
					toml.stringify({
						AlternativePriorityTitle: this.strAlternativePriorityTitle,
						TotalAlternativePriorityValue: 'false',
						TotalAlternativePriorityFormatAsMoney: 'false',

						AllowSFTPUploadAndDownload: this.strAllowSFTPUploadAndDownload,
						SFTPUsername: this.strSFTPUsername,
						SFTPServerName: this.strSFTPServerName,
						SFTPServerFolder: this.strSFTPServerFolder,
						GroupList: this.strArrGroupList,

						yearly: this.oDefaultYearItems,
						monthly: this.oDefaultMonthItems,
						biweek: this.oDefault2WeekItems,
						once: this.oDefaultOnceItems,
					}),
					{ flag: 'w+' },
				)
			}

			let strData: string = fs.readFileSync(this.strRecurringTomlFilename).toString()
			let oData = toml.parse(strData)
			this.oYearItems = <YearItem[]>oData.yearly
			this.oMonthItems = <MonthItem[]>oData.monthly
			this.o2WeekItems = <TwoWeekItem[]>oData.biweek
			this.oOnceItems = <OnceItem[]>oData.once
			if (oData.AlternativePriorityTitle)
				this.strAlternativePriorityTitle = <string>oData.AlternativePriorityTitle
			if (oData.TotalAlternativePriorityValue)
				this.strTotalAlternativePriorityValue = <string>oData.TotalAlternativePriorityValue

			if (oData.TotalAlternativePriorityFormatAsMoney)
				this.strTotalAlternativePriorityFormatAsMoney = <string>oData.TotalAlternativePriorityFormatAsMoney

			if (oData.AllowSFTPUploadAndDownload)
				this.strAllowSFTPUploadAndDownload = <string>oData.AllowSFTPUploadAndDownload
			if (oData.SFTPUsername) this.strSFTPUsername = <string>oData.SFTPUsername
			if (oData.SFTPServerName) this.strSFTPServerName = <string>oData.SFTPServerName
			if (oData.SFTPServerFolder) this.strSFTPServerFolder = <string>oData.SFTPServerFolder
			if (oData.GroupList) this.strArrGroupList = <string[]>oData.GroupList

			// console.log('read:\n' + strData)

			if (this.oYearItems && this.oYearItems.length && this.oYearItems.length > 1) {
				this.oYearItems.sort(
					(b, a) =>
						a.month * 100 + a.day < b.month * 100 + b.day
							? -1
							: a.month * 100 + a.day > b.month * 100 + b.day ? 1 : 0,
				)
			}

			if (this.oMonthItems && this.oMonthItems.length && this.oMonthItems.length > 1) {
				this.oMonthItems.sort((b, a) => (a.day < b.day ? -1 : a.day > b.day ? 1 : 0))
			}
		} catch (e) {
			console.log(e)
		}
	}

	async loadAndShowMonthTable(year: number, month: number): Promise<void> {
		let regexDatePattern = /^([012][0-9])$|^(3[0-1])$|^([1-9])$/
		this.intDisplayYear = year
		this.intDisplayMonth = month

		this.strRecordsTomlFilename =
			this.strDataTomlFolder +
			'/' +
			this.intDisplayYear.toString() +
			'-' +
			this.intDisplayMonth.toString().padStart(2, '0') +
			'-data' +
			this.strGroup.toLowerCase() +
			'.toml'

		this.oRecords = <RecordItem[]>new Array()
		if (fs.existsSync(this.strRecordsTomlFilename)) {
			let strData: string = fs.readFileSync(this.strRecordsTomlFilename).toString()
			let oData = toml.parse(strData).task
			if (oData) {
				this.oRecords = <RecordItem[]>oData
			}
		}

		// console.log('read: ' + toml.stringify({ task: this.oRecords }))
		await this.showTable()

		let strKey: string = ''
		let strMenu: string = ''
		strMenu += 'n) Next/Later, '
		strMenu += 'p) Previous/Ealier, '
		strMenu += 'e) Edit, '
		strMenu += 'a) Add, '
		strMenu += 'd) Delete, '
		strMenu += 'r) Show Recurring Items, '
		if (this.strAllowSFTPUploadAndDownload == 'true') {
			strMenu += 'g) Get Data from ' + this.strSFTPServerName + ', '
			strMenu += 's) Save Data to ' + this.strSFTPServerName + ', '
		}
		if (this.strArrGroupList && this.strArrGroupList.length > 0) {
			strMenu += 'c) Change Group, '
		}
		strMenu += 'f) Refresh, '
		strMenu += 'x) Exit'
		console.log(strMenu)
		if (this.strAllowSFTPUploadAndDownload == 'true' && this.strArrGroupList && this.strArrGroupList.length) {
			strKey = await this.askForKey('Letter (npeadrgscfx):')
		} else if (
			this.strAllowSFTPUploadAndDownload != 'true' &&
			this.strArrGroupList &&
			this.strArrGroupList.length
		) {
			strKey = await this.askForKey('Letter (npeadrfx):')
		} else if (
			this.strAllowSFTPUploadAndDownload == 'true' &&
			!(this.strArrGroupList && this.strArrGroupList.length)
		) {
			strKey = await this.askForKey('Letter (npeadrgsfx):')
		} else {
			strKey = await this.askForKey('Letter (npeadrfx):')
		}
		console.log('>' + strKey)
		process.stdin.setRawMode(false)

		let strPathDate: string = ''
		let dtmNow: Date = new Date()
		strPathDate += dtmNow.getFullYear().toString() + 'y-'
		strPathDate += (dtmNow.getMonth() + 1).toString().padStart(2, '0') + 'm-'
		strPathDate += dtmNow.getDate().toString().padStart(2, '0') + 'd--'
		strPathDate += dtmNow.getHours().toString().padStart(2, '0') + 'h-'
		strPathDate += dtmNow.getMinutes().toString().padStart(2, '0') + 'm-'
		strPathDate += dtmNow.getSeconds().toString().padStart(2, '0') + 's'
		let strLocalBackupDataRootFolderName: string = this.strDataTomlFolder + '-backups'
		let strLocalBackupDataFolderName: string = this.strDataTomlFolder + '-backups/' + strPathDate
		let strRemoteBackupDataFolderName: string = '~/' + this.strSFTPServerFolder + '-backups--' + strPathDate
		if (strKey == 'n') {
			this.oRecords = []
			this.intDisplayMonth++
			if (this.intDisplayMonth > 12) {
				this.intDisplayMonth = 1
				this.intDisplayYear++
			}
		} else if (
			strKey == 'c' &&
			this.strArrGroupList &&
			this.strArrGroupList.length &&
			this.strArrGroupList.length > 0
		) {
			let oItems: { letter: string; Name: string }[] = new Array()
			console.log('\n\nblank) Main Group')
			for (var intEachItem = 0; intEachItem < this.strArrGroupList.length; intEachItem++) {
				oItems.push({
					letter: String.fromCharCode(97 + intEachItem),
					Name: this.strArrGroupList[intEachItem],
				})
				console.log(String.fromCharCode(97 + intEachItem) + ') ' + this.strArrGroupList[intEachItem])
			}
			strKey = await this.askForKey('Letter:')
			if (!strKey) this.strGroup = ''
			for (let oGroupMenuItem of oItems) {
				if (strKey == oGroupMenuItem.letter) {
					this.strGroup = '-' + oGroupMenuItem.Name
				}
			}
			await this.loadRecurringData()
		} else if (strKey == 'g' && this.strAllowSFTPUploadAndDownload == 'true') {
			if (!fs.existsSync(strLocalBackupDataRootFolderName)) {
				fs.mkdirSync(strLocalBackupDataRootFolderName)
			}
			if (fs.existsSync(strLocalBackupDataFolderName + '/')) {
				console.error('Error: The folder ' + strLocalBackupDataFolderName + '/ already exists.')
				return
			} else {
				fs.renameSync(this.strDataTomlFolder, strLocalBackupDataFolderName + '/')
				fs.mkdirSync(this.strDataTomlFolder)

				// sftp  usrsftp@programmer1.com:code/ <<EOF
				// get bixfliz-sync-code.7z
				// exit
				// EOF

				execSync(
					'sftp ' +
						this.strSFTPUsername +
						'@' +
						this.strSFTPServerName +
						' <<EOF\n' +
						'cd ' +
						this.strSFTPServerFolder +
						'\n' +
						'lcd ' +
						this.strDataTomlFolder +
						'\n' +
						'mget * \n' +
						'exit\n' +
						'EOF\n',
					{ stdio: [ 0, 1, 2 ] },
				)
				// execSync(
				// 	'scp -r ' +
				// 		this.strSFTPUsername +
				// 		'@' +
				// 		this.strSFTPServerName +
				// 		':~/' +
				// 		this.strSFTPServerFolder +
				// 		' ' +
				// 		this.strDataTomlFolder +
				// 		'/',
				// 	{ stdio: [ 0, 1, 2 ] },
				// )
				await this.loadRecurringData()
			}
		} else if (strKey == 's' && this.strAllowSFTPUploadAndDownload == 'true') {
			execSync(
				'sftp ' +
					this.strSFTPUsername +
					'@' +
					this.strSFTPServerName +
					' <<EOF\n' +
					'cd ' +
					this.strSFTPServerFolder +
					'\n' +
					'lcd ' +
					this.strDataTomlFolder +
					'\n' +
					'mput * \n' +
					'cd  ' +
					strRemoteBackupDataFolderName +
					' \n' +
					'mput * \n' +
					'exit\n' +
					'EOF\n',
				{ stdio: [ 0, 1, 2 ] },
			)

			// execSync(
			// 	'scp -r ' +
			// 		this.strDataTomlFolder +
			// 		'/ ' +
			// 		this.strSFTPUsername +
			// 		'@' +
			// 		this.strSFTPServerName +
			// 		':' +
			// 		strRemoteBackupDataFolderName,
			// 	{ stdio: [ 0, 1, 2 ] },
			// )
			// execSync(
			// 	'scp -r ' +
			// 		this.strDataTomlFolder +
			// 		'/ ' +
			// 		this.strSFTPUsername +
			// 		'@' +
			// 		this.strSFTPServerName +
			// 		':~/' +
			// 		this.strSFTPServerFolder,
			// 	{ stdio: [ 0, 1, 2 ] },
			// )
		} else if (strKey == 'x') {
			// continue
		} else if (strKey == 'f') {
			// continue
		} else if (strKey == 'r') {
			await this.loadAndShowRecurringTable('start')
		} else if (strKey == 'p') {
			this.oRecords = []
			this.intDisplayMonth--
			if (this.intDisplayMonth == 0) {
				this.intDisplayMonth = 11
				this.intDisplayYear--
			}
		} else if (strKey == 'd') {
			let strEditRow = await this.askForKey('Row letter:')
			console.log('>' + strEditRow)
			process.stdin.setRawMode(false)
			for (var intEachItem = 0; intEachItem < this.oRecords.length; intEachItem++) {
				let oRec = this.oRecords[intEachItem]
				if (oRec.letter == strEditRow) {
					console.log('Delete: ' + oRec.title)
					let strConfirmDelete = await this.askForKey('Are sure? [yN]')
					console.log('>' + strConfirmDelete)
					if (strConfirmDelete == 'y' || strConfirmDelete == 'Y') {
						this.oRecords.splice(intEachItem, 1)
						await this.saveMonthRecords()
					}
					break
				}
			}
		} else if (strKey == 'e') {
			let strEditRow = await this.askForKey('Row letter:')
			console.log('>' + strEditRow)
			process.stdin.setRawMode(false)
			for (let oRec of this.oRecords) {
				if (oRec.letter == strEditRow) {
					let strEditDate: string = ''
					let strEditDateCompleted: string = ''
					let booValidDate: boolean = false
					let booValidDateCompleted: boolean = false
					do {
						strEditDate = readlineSync.question(
							'New day of the month for the due date: (' + oRec.due.substr(8, 2) + ') ',
							{
								defaultInput: oRec.due.substr(8, 2),
							},
						)
						if (regexDatePattern.test(strEditDate)) {
							booValidDate = true
						}
						if (!booValidDate) {
							console.log('Date must one or two digits.')
						}
					} while (!booValidDate)

					if (strEditDate) {
						if (strEditDate.length == 1) {
							let strFormattedDate: string = strEditDate
							strFormattedDate =
								this.intDisplayYear.toString() +
								'-' +
								this.intDisplayMonth.toString().padStart(2, '0') +
								'-0' +
								strEditDate
							strEditDate = strFormattedDate
						} else if (strEditDate.length == 2) {
							let strFormattedDate: string = strEditDate
							strFormattedDate =
								this.intDisplayYear.toString() +
								'-' +
								this.intDisplayMonth.toString().padStart(2, '0') +
								'-' +
								strEditDate
							strEditDate = strFormattedDate
						}
					}

					oRec.due = strEditDate
					do {
						strEditDateCompleted = readlineSync.question(
							'New day of the month for the date completed: (' + oRec.completed.substr(8, 2) + ') ',
							{
								defaultInput: oRec.completed.substr(8, 2),
							},
						)
						if (!strEditDateCompleted) {
							booValidDateCompleted = true
						}
						if (regexDatePattern.test(strEditDateCompleted)) {
							booValidDateCompleted = true
						}
						if (!booValidDateCompleted) {
							console.log('Date must one or two digits.')
						}
					} while (!booValidDateCompleted)

					if (strEditDateCompleted) {
						if (strEditDateCompleted.length == 1) {
							let strFormattedDate: string = strEditDateCompleted
							strFormattedDate =
								this.intDisplayYear.toString() +
								'-' +
								this.intDisplayMonth.toString().padStart(2, '0') +
								'-0' +
								strEditDateCompleted
							strEditDateCompleted = strFormattedDate
						} else if (strEditDateCompleted.length == 2) {
							let strFormattedDate: string = strEditDateCompleted
							strFormattedDate =
								this.intDisplayYear.toString() +
								'-' +
								this.intDisplayMonth.toString().padStart(2, '0') +
								'-' +
								strEditDateCompleted
							strEditDateCompleted = strFormattedDate
						}
					}
					oRec.completed = strEditDateCompleted
					let strEditStatus = readlineSync.question('New status: (' + oRec.status + ')', {
						defaultInput: oRec.status,
					})
					oRec.status = strEditStatus
					let strEditTitle = readlineSync.question('New title: (' + oRec.title + ')', {
						defaultInput: oRec.title,
					})
					oRec.title = strEditTitle
					let strEditPriority = readlineSync.question(
						'New ' + this.strAlternativePriorityTitle.toLowerCase() + ': (' + oRec.priority + ')',
						{
							defaultInput: oRec.priority,
						},
					)
					oRec.priority = strEditPriority
					let strEditNote = readlineSync.question('New note: (' + oRec.note + ')', {
						defaultInput: oRec.note,
					})
					oRec.note = strEditNote
					await this.saveMonthRecords()
					break
				}
			}
		} else if (strKey == 'a') {
			let strEditDate: string = ''
			let strEditDateCompleted: string = ''
			let booValidDate: boolean = false
			let booValidDateCompleted: boolean = false
			let oRec: RecordItem = <RecordItem>{}

			do {
				strEditDate = readlineSync.question('Due date day of the month: ')
				if (regexDatePattern.test(strEditDate)) {
					booValidDate = true
				}
				if (!booValidDate) {
					console.log('Date must be in the format dd or d.')
				}
			} while (!booValidDate)

			if (strEditDate) {
				if (strEditDate.length == 1) {
					let strFormattedDate: string = strEditDate
					strFormattedDate =
						this.intDisplayYear.toString() +
						'-' +
						this.intDisplayMonth.toString().padStart(2, '0') +
						'-0' +
						strEditDate
					strEditDate = strFormattedDate
				} else if (strEditDate.length == 2) {
					let strFormattedDate: string = strEditDate
					strFormattedDate =
						this.intDisplayYear.toString() +
						'-' +
						this.intDisplayMonth.toString().padStart(2, '0') +
						'-' +
						strEditDate
					strEditDate = strFormattedDate
				}
			}
			oRec.due = strEditDate

			do {
				strEditDateCompleted = readlineSync.question('Date completed day of the month: ')
				if (!strEditDateCompleted) {
					booValidDateCompleted = true
				}
				if (regexDatePattern.test(strEditDateCompleted)) {
					booValidDateCompleted = true
				}
				if (!booValidDateCompleted) {
					console.log('Date must be in the format dd or d.')
				}
			} while (!booValidDateCompleted)

			if (strEditDateCompleted) {
				if (strEditDateCompleted.length == 1) {
					let strFormattedDate: string = strEditDateCompleted
					strFormattedDate =
						this.intDisplayYear.toString() +
						'-' +
						this.intDisplayMonth.toString().padStart(2, '0') +
						'-0' +
						strEditDateCompleted
					strEditDateCompleted = strFormattedDate
				} else if (strEditDateCompleted.length == 2) {
					let strFormattedDate: string = strEditDateCompleted
					strFormattedDate =
						this.intDisplayYear.toString() +
						'-' +
						this.intDisplayMonth.toString().padStart(2, '0') +
						'-' +
						strEditDateCompleted
					strEditDateCompleted = strFormattedDate
				}
			}
			oRec.completed = strEditDateCompleted

			let strEditStatus = readlineSync.question('Status: ')
			oRec.status = strEditStatus
			let strEditTitle = readlineSync.question('Title: ')
			oRec.title = strEditTitle
			let strEditPriority = readlineSync.question(this.strAlternativePriorityTitle + ': ')
			oRec.priority = strEditPriority
			let strEditNote = readlineSync.question('Note: ')
			oRec.note = strEditNote
			this.oRecords.push(oRec)
			await this.saveMonthRecords()
		} else {
			console.log('Invalid key.')
		}

		if (strKey != 'x') {
			await this.loadAndShowMonthTable(this.intDisplayYear, this.intDisplayMonth)
		}
	}

	async loadAndShowRecurringTable(recurringEditMode: 'start' | 'year' | 'month' | '2week' | 'once'): Promise<void> {
		let regexDatePattern = /^([012][0-9])$|^(3[0-1])$|^([1-9])$/

		let oYearTableHeaders = [
			'',
			'Title',
			'Date',
			'Default\n' + this.strAlternativePriorityTitle,
			'Default\nStatus',
			'Default\nNote',
		]
		let oYearTableColWidths = [ 3, 27, 7, 14, 10, 40 ]
		let oMonthTableHeaders = [
			'',
			'Title',
			'Day',
			'Default\n' + this.strAlternativePriorityTitle,
			'Default\nStatus',
			'Default\nNote',
		]
		let oMonthTableColWidths = [ 3, 22, 18, 14, 10, 40 ]
		let o2WeekTableHeaders = [
			'',
			'Title',
			'Start\nDate',
			'Default\n' + this.strAlternativePriorityTitle,
			'Default\nStatus',
			'Default\nNote',
		]
		let o2WeekTableColWidths = [ 3, 22, 18, 14, 10, 40 ]
		let oOnceTableHeaders = [
			'',
			'Title',
			'Date',
			'Default\n' + this.strAlternativePriorityTitle,
			'Default\nStatus',
			'Default\nNote',
		]
		let oOnceTableColWidths = [ 3, 22, 18, 14, 10, 40 ]

		let strKey: string = ''
		let strInputValue: string = ''

		if (recurringEditMode == 'start') {
			console.log(
				'y) Show Yearly Items, ' +
					'm) Show Monthy Items, ' +
					'b) Show Bi-Weekly Items, ' +
					'o) Show One Time Items, ' +
					'f) Refresh, ' +
					'x) Exit',
			)
			strKey = await this.askForKey('Letter:')
		} else {
			await this.loadRecurringData()
			let oCliTable: Table.HorizontalTable
			if (recurringEditMode == 'year') {
				oCliTable = new Table({
					head: oYearTableHeaders,
					colWidths: oYearTableColWidths,
					wordWrap: true,
				}) as Table.HorizontalTable
				for (var intEachRecord = 0; intEachRecord < this.oYearItems.length; intEachRecord++) {
					let oRow = this.oYearItems[intEachRecord]
					this.oYearItems[intEachRecord].letter = String.fromCharCode(97 + intEachRecord)

					oCliTable.push([
						String.fromCharCode(97 + intEachRecord),
						oRow.title,
						oRow.month.toString().padStart(2, '0') + '/' + oRow.day.toString().padStart(2, '0'),
						oRow.defaultPriority,
						oRow.defaultStatus,
						oRow.defaultNote,
					])
				}
				console.log(chalk.default.red.bold.underline('\n\n' + 'Yealy'))
			} else if (recurringEditMode == 'month') {
				oCliTable = new Table({
					head: oMonthTableHeaders,
					colWidths: oMonthTableColWidths,
					wordWrap: true,
				}) as Table.HorizontalTable

				for (var intEachRecord = 0; intEachRecord < this.oMonthItems.length; intEachRecord++) {
					let oRow = this.oMonthItems[intEachRecord]
					this.oMonthItems[intEachRecord].letter = String.fromCharCode(97 + intEachRecord)

					oCliTable.push([
						String.fromCharCode(97 + intEachRecord),
						oRow.title,
						oRow.day.toString().padStart(2, '0'),
						oRow.defaultPriority,
						oRow.defaultStatus,
						oRow.defaultNote,
					])
				}
				console.log(chalk.default.red.bold.underline('\n\n' + 'Daily'))
			} else if (recurringEditMode == '2week') {
				oCliTable = new Table({
					head: o2WeekTableHeaders,
					colWidths: o2WeekTableColWidths,
					wordWrap: true,
				}) as Table.HorizontalTable

				for (var intEachRecord = 0; intEachRecord < this.o2WeekItems.length; intEachRecord++) {
					let oRow = this.o2WeekItems[intEachRecord]
					this.o2WeekItems[intEachRecord].letter = String.fromCharCode(97 + intEachRecord)

					let dtmEachDate: Date = new Date(Date.parse(oRow.startDate))
					oCliTable.push([
						String.fromCharCode(97 + intEachRecord),
						oRow.title,
						(dtmEachDate.getUTCMonth() + 1).toString() +
							'/' +
							dtmEachDate.getUTCDate().toString() +
							'/' +
							dtmEachDate.getUTCFullYear().toString() +
							' ' +
							this.dayName(dtmEachDate),
						oRow.defaultPriority,
						oRow.defaultStatus,
						oRow.defaultNote,
					])
				}
				console.log(chalk.default.red.bold.underline('\n\n' + 'Bi-Weekly (every other week)'))
			} else if (recurringEditMode == 'once') {
				oCliTable = new Table({
					head: oOnceTableHeaders,
					colWidths: oOnceTableColWidths,
					wordWrap: true,
				}) as Table.HorizontalTable

				for (var intEachRecord = 0; intEachRecord < this.oOnceItems.length; intEachRecord++) {
					let oRow = this.oOnceItems[intEachRecord]
					this.oOnceItems[intEachRecord].letter = String.fromCharCode(97 + intEachRecord)

					let dtmEachDate: Date = new Date(Date.parse(oRow.date))
					oCliTable.push([
						String.fromCharCode(97 + intEachRecord),
						oRow.title,
						(dtmEachDate.getUTCMonth() + 1).toString() +
							'/' +
							dtmEachDate.getUTCDate().toString() +
							'/' +
							dtmEachDate.getUTCFullYear().toString() +
							' ' +
							this.dayName(dtmEachDate),
						oRow.defaultPriority,
						oRow.defaultStatus,
						oRow.defaultNote,
					])
				}
				console.log(chalk.default.red.bold.underline('\n\n' + 'One Time'))
			}
			console.log(oCliTable.toString())
			console.log(
				'e) Edit, ' + 'a) Add, ' + 'd) Delete, ' + 'r) Show Record Items, ' + 'f) Refresh, ' + 'x) Exit',
			)
			strKey = await this.askForKey('Letter:')
		}
		console.log('>' + strKey)

		if (strKey) {
			if (recurringEditMode == 'start') {
				if (strKey == 'y') {
					await this.loadAndShowRecurringTable('year')
				} else if (strKey == 'm') {
					await this.loadAndShowRecurringTable('month')
				} else if (strKey == 'b') {
					await this.loadAndShowRecurringTable('2week')
				} else if (strKey == 'o') {
					await this.loadAndShowRecurringTable('once')
				}
				return
			} else if (
				recurringEditMode == 'year' ||
				recurringEditMode == 'month' ||
				recurringEditMode == '2week' ||
				recurringEditMode == 'once'
			) {
				if (strKey == 'e' && recurringEditMode == 'year') {
					let strEditRow = await this.askForKey('Row letter:')
					console.log('>' + strEditRow)
					for (let oRec of this.oYearItems) {
						if (oRec.letter == strEditRow) {
							strInputValue = readlineSync.question('New title: (' + oRec.title + ')', {
								defaultInput: oRec.title,
							})
							oRec.title = strInputValue

							do {
								strInputValue = readlineSync.question('New month: (' + oRec.month.toString() + ')', {
									defaultInput: oRec.month.toString().padStart(2, '0'),
								})
								oRec.month = Number.parseInt(strInputValue)
							} while (!(oRec.month > 0) || !(oRec.month < 13))

							do {
								strInputValue = readlineSync.question('New day: (' + oRec.day.toString() + ')', {
									defaultInput: oRec.day.toString(),
								})
								oRec.day = Number.parseInt(strInputValue)
							} while (!(oRec.day > 0) || !(oRec.day < 32))

							strInputValue = readlineSync.question(
								'New ' +
									this.strAlternativePriorityTitle.toLowerCase() +
									': (' +
									oRec.defaultPriority +
									')',
								{
									defaultInput: oRec.defaultPriority,
								},
							)
							oRec.defaultPriority = strInputValue

							strInputValue = readlineSync.question('New status: (' + oRec.defaultStatus + ')', {
								defaultInput: oRec.defaultStatus,
							})
							oRec.defaultStatus = strInputValue

							strInputValue = readlineSync.question('New note: (' + oRec.defaultNote + ')', {
								defaultInput: oRec.defaultNote,
							})
							oRec.defaultNote = strInputValue
							break
						}
					}
					this.saveRecurringRecords()
					await this.loadAndShowRecurringTable(recurringEditMode)
				} else if (strKey == 'e' && recurringEditMode == 'month') {
					let strEditRow = await this.askForKey('Row letter:')
					console.log('>' + strEditRow)
					for (let oRec of this.oMonthItems) {
						if (oRec.letter == strEditRow) {
							strInputValue = readlineSync.question('New title: (' + oRec.title + ')', {
								defaultInput: oRec.title,
							})
							oRec.title = strInputValue

							do {
								strInputValue = readlineSync.question('New day: (' + oRec.day.toString() + ')', {
									defaultInput: oRec.day.toString(),
								})
								oRec.day = Number.parseInt(strInputValue)
							} while (!(oRec.day > 0) || !(oRec.day < 32))

							strInputValue = readlineSync.question(
								'New ' +
									this.strAlternativePriorityTitle.toLowerCase() +
									': (' +
									oRec.defaultPriority +
									')',
								{
									defaultInput: oRec.defaultPriority,
								},
							)
							oRec.defaultPriority = strInputValue

							strInputValue = readlineSync.question('New status: (' + oRec.defaultStatus + ')', {
								defaultInput: oRec.defaultStatus,
							})
							oRec.defaultStatus = strInputValue

							strInputValue = readlineSync.question('New note: (' + oRec.defaultNote + ')', {
								defaultInput: oRec.defaultNote,
							})
							oRec.defaultNote = strInputValue
							break
						}
					}
					this.saveRecurringRecords()
					await this.loadAndShowRecurringTable(recurringEditMode)
				} else if (strKey == 'e' && recurringEditMode == '2week') {
					let strEditRow = await this.askForKey('Row letter:')
					console.log('>' + strEditRow)
					for (let oRec of this.o2WeekItems) {
						if (oRec.letter == strEditRow) {
							strInputValue = readlineSync.question('New title: (' + oRec.title + ')', {
								defaultInput: oRec.title,
							})
							oRec.title = strInputValue

							let dtmAddStartDate: Date
							let booValidDate: boolean = false
							do {
								strInputValue = readlineSync.question('New date: (' + oRec.startDate + ')', {
									defaultInput: oRec.startDate,
								})
								dtmAddStartDate = new Date(Date.parse(strInputValue))
								oRec.startDate =
									dtmAddStartDate.getUTCFullYear().toString() +
									'-' +
									(dtmAddStartDate.getUTCMonth() + 1).toString().padStart(2, '0') +
									'-' +
									dtmAddStartDate.getUTCDate().toString().padStart(2, '0')
								if (dtmAddStartDate instanceof Date && !isNaN(Number(dtmAddStartDate)))
									booValidDate = true
							} while (booValidDate)

							strInputValue = readlineSync.question(
								'New ' +
									this.strAlternativePriorityTitle.toLowerCase() +
									': (' +
									oRec.defaultPriority +
									')',
								{
									defaultInput: oRec.defaultPriority,
								},
							)
							oRec.defaultPriority = strInputValue

							strInputValue = readlineSync.question('New status: (' + oRec.defaultStatus + ')', {
								defaultInput: oRec.defaultStatus,
							})
							oRec.defaultStatus = strInputValue

							strInputValue = readlineSync.question('New note: (' + oRec.defaultNote + ')', {
								defaultInput: oRec.defaultNote,
							})
							oRec.defaultNote = strInputValue
							break
						}
					}
					this.saveRecurringRecords()
					await this.loadAndShowRecurringTable(recurringEditMode)
				} else if (strKey == 'e' && recurringEditMode == 'once') {
					let strEditRow = await this.askForKey('Row letter:')
					console.log('>' + strEditRow)
					for (let oRec of this.oOnceItems) {
						if (oRec.letter == strEditRow) {
							strInputValue = readlineSync.question('New title: (' + oRec.title + ')', {
								defaultInput: oRec.title,
							})
							oRec.title = strInputValue

							let dtmAddStartDate: Date
							let booValidDate: boolean = false
							do {
								strInputValue = readlineSync.question('New date: (' + oRec.date + ')', {
									defaultInput: oRec.date,
								})
								dtmAddStartDate = new Date(Date.parse(strInputValue))
								oRec.date =
									dtmAddStartDate.getUTCFullYear().toString() +
									'-' +
									(dtmAddStartDate.getUTCMonth() + 1).toString().padStart(2, '0') +
									'-' +
									dtmAddStartDate.getUTCDate().toString().padStart(2, '0')
								if (dtmAddStartDate instanceof Date && !isNaN(Number(dtmAddStartDate)))
									booValidDate = true
							} while (booValidDate)

							strInputValue = readlineSync.question(
								'New ' +
									this.strAlternativePriorityTitle.toLowerCase() +
									': (' +
									oRec.defaultPriority +
									')',
								{
									defaultInput: oRec.defaultPriority,
								},
							)
							oRec.defaultPriority = strInputValue

							strInputValue = readlineSync.question('New status: (' + oRec.defaultStatus + ')', {
								defaultInput: oRec.defaultStatus,
							})
							oRec.defaultStatus = strInputValue

							strInputValue = readlineSync.question('New note: (' + oRec.defaultNote + ')', {
								defaultInput: oRec.defaultNote,
							})
							oRec.defaultNote = strInputValue
							break
						}
					}
					this.saveRecurringRecords()
					await this.loadAndShowRecurringTable(recurringEditMode)
				} else if (strKey == 'a' && recurringEditMode == 'year') {
					let oRec: YearItem = <YearItem>{}

					strInputValue = readlineSync.question('Title: ')
					oRec.title = strInputValue

					do {
						strInputValue = readlineSync.question('Month: ')
						oRec.month = Number.parseInt(strInputValue)
					} while (!(oRec.month > 0) || !(oRec.month < 13))

					do {
						strInputValue = readlineSync.question('Day: ')
						oRec.day = Number.parseInt(strInputValue)
					} while (!(oRec.day > 0) || !(oRec.day < 32))

					strInputValue = readlineSync.question('Status: ')
					oRec.defaultStatus = strInputValue
					strInputValue = readlineSync.question(this.strAlternativePriorityTitle + ': ')
					oRec.defaultPriority = strInputValue
					strInputValue = readlineSync.question('Note: ')
					oRec.defaultNote = strInputValue
					this.oYearItems.push(oRec)
					this.saveRecurringRecords()
					await this.loadAndShowRecurringTable(recurringEditMode)
				} else if (strKey == 'a' && recurringEditMode == 'month') {
					let oRec: MonthItem = <MonthItem>{}

					strInputValue = readlineSync.question('Title: ')
					oRec.title = strInputValue
					do {
						strInputValue = readlineSync.question('Day: ')
						oRec.day = Number.parseInt(strInputValue)
					} while (!(oRec.day > 0) || !(oRec.day < 32))
					strInputValue = readlineSync.question('Status: ')
					oRec.defaultStatus = strInputValue
					strInputValue = readlineSync.question(this.strAlternativePriorityTitle + ': ')
					oRec.defaultPriority = strInputValue
					strInputValue = readlineSync.question('Note: ')
					oRec.defaultNote = strInputValue
					this.oMonthItems.push(oRec)
					this.saveRecurringRecords()
					await this.loadAndShowRecurringTable(recurringEditMode)
				} else if (strKey == 'a' && recurringEditMode == '2week') {
					let oRec: TwoWeekItem = <TwoWeekItem>{}

					strInputValue = readlineSync.question('Title: ')
					oRec.title = strInputValue

					let dtmStartDate: Date
					let booValidDate: boolean = false
					do {
						strInputValue = readlineSync.question('Start Date: ')
						dtmStartDate = new Date(Date.parse(strInputValue))
						oRec.startDate =
							dtmStartDate.getUTCFullYear().toString() +
							'-' +
							(dtmStartDate.getUTCMonth() + 1).toString().padStart(2, '0') +
							'-' +
							dtmStartDate.getUTCDate().toString().padStart(2, '0')
						if (dtmStartDate instanceof Date && !isNaN(Number(dtmStartDate))) booValidDate = true
					} while (booValidDate)

					strInputValue = readlineSync.question('Status: ')
					oRec.defaultStatus = strInputValue
					strInputValue = readlineSync.question(this.strAlternativePriorityTitle + ': ')
					oRec.defaultPriority = strInputValue
					strInputValue = readlineSync.question('Note: ')
					oRec.defaultNote = strInputValue
					this.o2WeekItems.push(oRec)
					this.saveRecurringRecords()
					await this.loadAndShowRecurringTable(recurringEditMode)
				} else if (strKey == 'a' && recurringEditMode == 'once') {
					let oRec: OnceItem = <OnceItem>{}

					strInputValue = readlineSync.question('Title: ')
					oRec.title = strInputValue

					let dtmStartDate: Date
					let booValidDate: boolean = false
					do {
						strInputValue = readlineSync.question('Date: ')
						dtmStartDate = new Date(Date.parse(strInputValue))
						oRec.date =
							dtmStartDate.getUTCFullYear().toString() +
							'-' +
							(dtmStartDate.getUTCMonth() + 1).toString().padStart(2, '0') +
							'-' +
							dtmStartDate.getUTCDate().toString().padStart(2, '0')
						if (dtmStartDate instanceof Date && !isNaN(Number(dtmStartDate))) booValidDate = true
					} while (booValidDate)

					strInputValue = readlineSync.question('Status: ')
					oRec.defaultStatus = strInputValue
					strInputValue = readlineSync.question(this.strAlternativePriorityTitle + ': ')
					oRec.defaultPriority = strInputValue
					strInputValue = readlineSync.question('Note: ')
					oRec.defaultNote = strInputValue
					this.oOnceItems.push(oRec)
					this.saveRecurringRecords()
					await this.loadAndShowRecurringTable(recurringEditMode)
				} else if (strKey == 'd') {
					let strEditRow = await this.askForKey('Row letter:')
					console.log('>' + strEditRow)
					process.stdin.setRawMode(false)
					let oData: YearItem[] | MonthItem[] | TwoWeekItem[] | OnceItem[]
					if (recurringEditMode == 'year') oData = this.oYearItems
					if (recurringEditMode == 'month') oData = this.oMonthItems
					if (recurringEditMode == '2week') oData = this.o2WeekItems
					if (recurringEditMode == 'once') oData = this.oOnceItems
					for (var intEachItem = 0; intEachItem < oData.length; intEachItem++) {
						let oRec = oData[intEachItem]
						if (oRec.letter == strEditRow) {
							console.log('Delete: ' + oRec.title)
							let strConfirmDelete = await this.askForKey('Are sure? [yN]')
							console.log('>' + strConfirmDelete)
							if (strConfirmDelete == 'y' || strConfirmDelete == 'Y') {
								if (recurringEditMode == 'year') {
									this.oYearItems.splice(intEachItem, 1)
									for (let oRow of this.oYearItems) {
										delete oRow.letter
									}
								} else if (recurringEditMode == 'month') {
									this.oMonthItems.splice(intEachItem, 1)
									for (let oRow of this.oMonthItems) {
										delete oRow.letter
									}
								} else if (recurringEditMode == '2week') {
									this.o2WeekItems.splice(intEachItem, 1)
									for (let oRow of this.o2WeekItems) {
										delete oRow.letter
									}
								} else if (recurringEditMode == 'once') {
									this.oOnceItems.splice(intEachItem, 1)
									for (let oRow of this.oOnceItems) {
										delete oRow.letter
									}
								}
							}
							break
						}
					}
					this.saveRecurringRecords()
					await this.loadAndShowRecurringTable(recurringEditMode)
				}
			}
			if (strKey == 'f') {
				await this.loadAndShowRecurringTable(recurringEditMode)
			} else if (strKey == 'x') {
				return
			}
		}
	}

	async saveRecurringRecords() {
		for (let oRow of this.oYearItems) {
			delete oRow.letter
		}
		for (let oRow of this.oMonthItems) {
			delete oRow.letter
		}
		for (let oRow of this.o2WeekItems) {
			delete oRow.letter
		}
		for (let oRow of this.oOnceItems) {
			delete oRow.letter
		}
		fs.writeFileSync(
			this.strRecurringTomlFilename,
			toml.stringify({
				AlternativePriorityTitle: this.strAlternativePriorityTitle,
				TotalAlternativePriorityValue: this.strTotalAlternativePriorityValue,
				TotalAlternativePriorityFormatAsMoney: this.strTotalAlternativePriorityFormatAsMoney,

				AllowSFTPUploadAndDownload: this.strAllowSFTPUploadAndDownload,
				SFTPUsername: this.strSFTPUsername,
				SFTPServerName: this.strSFTPServerName,
				SFTPServerFolder: this.strSFTPServerFolder,

				GroupList: this.strArrGroupList,

				yearly: this.oYearItems,
				monthly: this.oMonthItems,
				biweek: this.o2WeekItems,
				once: this.oOnceItems,
			}),
			{ flag: 'w+' },
		)
	}

	async saveMonthRecords() {
		this.strRecordsTomlFilename =
			this.strDataTomlFolder +
			'/' +
			this.intDisplayYear.toString() +
			'-' +
			this.intDisplayMonth.toString().padStart(2, '0') +
			'-data' +
			this.strGroup.toLowerCase() +
			'.toml'

		if (this.oRecords && this.oRecords.length && this.oRecords.length > 0) {
			let task: RecordItem[] = this.oRecords
			for (let oRow of this.oRecords) {
				delete oRow.letter
			}

			fs.writeFileSync(this.strRecordsTomlFilename, toml.stringify({ task }), { flag: 'w+' })
		}
		// console.log('wrote: ' + toml.stringify({ task }))
	}

	askForKey(msg: string): Promise<string> {
		if (msg) console.log(msg)

		const stdin = process.stdin
		stdin.setRawMode(true)

		stdin.resume()

		return new Promise((resolve, reject) => {
			stdin.on('data', (key: any) => {
				stdin.pause()

				resolve(key.toString('utf8'))
				stdin.setRawMode(false)
			})
		})
	}

	dayName(dtmDate: Date) {
		let days = [ 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat' ]
		return days[dtmDate.getUTCDay()]
	}

	monthName(numMonth: number) {
		const monthNames = [
			'January',
			'February',
			'March',
			'April',
			'May',
			'June',
			'July',
			'August',
			'September',
			'October',
			'November',
			'December',
		]
		return monthNames[numMonth - 1]
	}
}

new BillProg()
